package pointer

func ToUintPointer(i uint) *uint {
	return &i
}
