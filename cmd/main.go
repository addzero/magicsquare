package main

import (
	"addzero/bigpay/magicsquare/internal/models"
	"fmt"
	"log"
	"sync"

	"github.com/jinzhu/copier"
)

const sum uint = 34

func isEnd(row, column uint, data models.Data) bool {
	size := uint(len(data.Board))
	if row == size-1 && column == size-1 {
		return true
	}
	return false
}

func validate(row, column uint, data models.Data) (bool, error) {
	size := uint(len(data.Board))
	if row+1 > size || column+1 > size {
		return false, fmt.Errorf("invalid row or column")
	}

	if column == 3 && data.Sum.Rows[row] != sum ||
		row == 3 && data.Sum.Columns[column] != sum ||
		row == 3 && column == 0 && data.Sum.Diagonal[1] != sum ||
		row == 3 && column == 3 && data.Sum.Diagonal[0] != sum {
		return false, nil
	}

	if row == 1 && column == 1 && data.Sum.Columns[0]+data.Sum.Columns[1] != sum {
		return false, nil
	}

	if row == 1 && column == 3 && data.Sum.Columns[2]+data.Sum.Columns[3] != sum {
		return false, nil
	}

	if data.Sum.Rows[row] > sum ||
		data.Sum.Columns[column] > sum ||
		(row == column && data.Sum.Diagonal[0] > sum) ||
		(row+column+1 == size && data.Sum.Diagonal[1] > sum) {
		return false, nil
	}
	return true, nil
}

func getNextRowColumnData(row, column uint, data models.Data) (*uint, *uint, *models.Data, error) {
	var localData models.Data
	copyErr := copier.Copy(&localData, &data)
	if copyErr != nil {
		return nil, nil, nil, copyErr
	}

	size := uint(len(data.Board))
	if row == size-1 && column == size-1 {
		return nil, nil, &localData, nil
	}

	var nextRow, nextColumn uint
	if column+1 == size {
		nextRow = row + 1
		nextColumn = 0
	} else {
		nextRow = row
		nextColumn = column + 1
	}

	return &nextRow, &nextColumn, &localData, nil
}

func doPick(row, column, pickAt uint, data models.Data) (*models.Data, error) {
	nextRow, nextColumn, nextData, nextErr := getNextRowColumnData(row, column, data)
	pick := nextData.Remain[pickAt]
	// start assign picked to data
	nextData.Board[row][column] = pick
	rem := make([]uint, len(nextData.Remain))
	copy(rem, nextData.Remain)
	nextData.Remain = append(rem[:pickAt], rem[pickAt+1:]...)
	nextData.Sum.Rows[row] += pick
	nextData.Sum.Columns[column] += pick
	if row == column {
		nextData.Sum.Diagonal[0] += pick
	}

	if column+row+1 == uint(len(nextData.Board)) {
		nextData.Sum.Diagonal[1] += pick
	}
	// end assign picked to data

	if isValid, validErr := validate(row, column, *nextData); validErr != nil || !isValid {
		return nextData, fmt.Errorf("invalid picked")
	}

	if nextErr != nil {
		return nextData, nextErr
	}

	if isEnd(row, column, data) {
		log.Printf("\nYeah! the magic square for you\n-------------------------------")
		nextData.Board.Print()
		return nextData, nil
	}

	if nextRow == nil || nextColumn == nil || nextData == nil {
		return nextData, fmt.Errorf("next data cannot be null")
	}

	var wg sync.WaitGroup
	for idx := range nextData.Remain {
		wg.Add(1)
		go func(i uint) {
			defer wg.Done()
			_, pickErr := doPick(*nextRow, *nextColumn, i, *nextData)
			if pickErr != nil {
				return
			}
		}(uint(idx))
	}
	wg.Wait()
	return nextData, fmt.Errorf("error")
}

func main() {
	remain := []uint{2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
	board := [4][4]uint{
		{1, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0},
	}
	sum := models.Sum{
		Rows:     [4]uint{1, 0, 0, 0},
		Columns:  [4]uint{1, 0, 0, 0},
		Diagonal: [2]uint{1, 0},
	}
	masterData := models.Data{
		Board:  board,
		Remain: remain,
		Sum:    sum,
	}

	wg := sync.WaitGroup{}
	for idx := 0; idx < 4; idx++ {
		wg.Add(1)
		go func(i uint) {
			defer wg.Done()
			resultData, pickErr := doPick(0, 1, i, masterData)
			if pickErr == nil {
				log.Println(resultData)
			}
		}(uint(idx))
	}
	wg.Wait()
}
