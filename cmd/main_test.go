package main

import (
	. "addzero/bigpay/magicsquare/internal/models"
	"addzero/bigpay/magicsquare/shared/pointer"
	"reflect"
	"testing"
)

func Test_getNextRowColumnData(t *testing.T) {
	mockData := Data{
		Board:  Board{},
		Remain: nil,
		Sum:    Sum{},
	}
	type args struct {
		row    uint
		column uint
		data   Data
	}
	tests := []struct {
		name       string
		args       args
		wantRow    *uint
		wantColumn *uint
		wantData   *Data
		wantErr    bool
	}{
		{
			name: "should get same row next column if row 0 column 0",
			args: args{
				row:    0,
				column: 0,
				data:   mockData,
			},
			wantRow:    pointer.ToUintPointer(0),
			wantColumn: pointer.ToUintPointer(1),
			wantData:   &mockData,
		},
		{
			name: "should get next row reset column if row 0 column 3",
			args: args{
				row:    0,
				column: 3,
			},
			wantRow:    pointer.ToUintPointer(1),
			wantColumn: pointer.ToUintPointer(0),
			wantData:   &mockData,
		},
		{
			name: "should get nil row nil column if row 3 column 3",
			args: args{
				row:    3,
				column: 3,
			},
			wantRow:    nil,
			wantColumn: nil,
			wantData:   &mockData,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRow, gotColumn, gotData, err := getNextRowColumnData(tt.args.row, tt.args.column, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("getNextRowColumnData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotRow, tt.wantRow) {
				t.Errorf("getNextRowColumnData() gotRow = %v, wantRow %v", gotRow, tt.wantRow)
			}
			if !reflect.DeepEqual(gotColumn, tt.wantColumn) {
				t.Errorf("getNextRowColumnData() gotColumn = %v, wantColumn %v", gotColumn, tt.wantColumn)
			}
			if !reflect.DeepEqual(gotData, tt.wantData) {
				t.Errorf("getNextRowColumnData() gotData = %v, wantData %v", gotData, tt.wantData)
			}
		})
	}
}

func Test_validate(t *testing.T) {
	type args struct {
		row    uint
		column uint
		data   Data
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "should invalid if diagonal 2 invalid when row 3 column 0 (lower left)",
			args: args{
				row:    3,
				column: 0,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{0, 42},
					},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if diagonal 1 invalid when row 0 column 0 (upper left)",
			args: args{
				row:    0,
				column: 0,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{42, 16},
					},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if diagonal 1 invalid",
			args: args{
				row:    1,
				column: 1,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{42, 16},
					},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if sum of 4 upper left tiles not equal to 34",
			args: args{
				row:    1,
				column: 1,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{15, 16},
					},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if sum of 4 upper right tiles not equal to 34",
			args: args{
				row:    1,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 34, 0, 0},
						Columns:  [4]uint{16, 18, 20, 13},
						Diagonal: [2]uint{15, 16},
					},
				},
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if row greater than board size",
			args: args{
				row:    4,
				column: 3,
				data: Data{
					Board: [4][4]uint{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{42, 16},
					},
				},
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "should invalid if column greater than board size",
			args: args{
				row:    3,
				column: 4,
				data: Data{
					Board: [4][4]uint{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
					Sum: Sum{
						Rows:     [4]uint{0, 0, 0, 16},
						Columns:  [4]uint{16, 0, 0, 0},
						Diagonal: [2]uint{42, 16},
					},
				},
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "should valid if row 3 column 3 and sum of all row, column and diagonal equal to 34",
			args: args{
				row:    3,
				column: 3,
				data: Data{
					Board: [4][4]uint{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
					Sum: Sum{
						Rows:     [4]uint{34, 34, 34, 34},
						Columns:  [4]uint{34, 34, 34, 34},
						Diagonal: [2]uint{34, 34},
					},
				},
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "should valid if row 3 column 1 and sum of row 3 less than 34 sum of column 1 equal to 34",
			args: args{
				row:    3,
				column: 1,
				data: Data{
					Board: [4][4]uint{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}},
					Sum: Sum{
						Rows:     [4]uint{34, 34, 34, 20},
						Columns:  [4]uint{34, 34, 28, 30},
						Diagonal: [2]uint{26, 34},
					},
				},
			},
			want:    true,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := validate(tt.args.row, tt.args.column, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("validate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isEnd(t *testing.T) {
	type args struct {
		row    uint
		column uint
		data   Data
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "should return end if row = 3 and column = 3 and board size is 4",
			args: args{
				row:    3,
				column: 3,
			},
			want: true,
		},
		{
			name: "should return not end if row = 3 and column = 2 and board size is 4",
			args: args{
				row:    3,
				column: 2,
			},
			want: false,
		},
		{
			name: "should return not end if row = 0 and column = 0 and board size is 4",
			args: args{
				row:    0,
				column: 0,
			},
			want: false,
		},
		{
			name: "should return not end if row = 2 and column = 2 and board size is 4",
			args: args{
				row:    2,
				column: 2,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isEnd(tt.args.row, tt.args.column, tt.args.data); got != tt.want {
				t.Errorf("isEnd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_validate1(t *testing.T) {
	type args struct {
		row    uint
		column uint
		data   Data
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "should error if row greater than board size - 1",
			args: args{
				row:    4,
				column: 0,
			},
			wantErr: true,
		},
		{
			name: "should error if column greater than board size - 1",
			args: args{
				row:    0,
				column: 4,
			},
			wantErr: true,
		},
		{
			name: "should error if both row and column greater than board size - 1",
			args: args{
				row:    4,
				column: 4,
			},
			wantErr: true,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of row 0 is not 34",
			args: args{
				row:    0,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{15, 0, 0, 0},
						Columns:  [4]uint{1, 2, 3, 4},
						Diagonal: [2]uint{15, 0},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of row 0 is not 34",
			args: args{
				row:    1,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 14, 0, 0},
						Columns:  [4]uint{10, 20, 30, 16},
						Diagonal: [2]uint{20, 0},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of row 0 is not 34",
			args: args{
				row:    2,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 34, 20, 0},
						Columns:  [4]uint{20, 24, 30, 32},
						Diagonal: [2]uint{20, 0},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of row 0 is not 34",
			args: args{
				row:    3,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:    [4]uint{34, 34, 34, 20},
						Columns: [4]uint{20, 24, 30, 32},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should valid if column is 3 and row 0 and sum of row 0 not 34",
			args: args{
				row:    0,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 0, 0, 0},
						Columns:  [4]uint{1, 4, 16, 13},
						Diagonal: [2]uint{1, 13},
					},
				}},
			want:    true,
			wantErr: false,
		},
		{
			name: "should invalid if column is 1 and row 1 and sum of column 0 and column 1 is not 34",
			args: args{
				row:    1,
				column: 1,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 10, 0, 0},
						Columns:  [4]uint{20, 4, 16, 13},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should valid if column is 1 and row 1 and sum of column 0 and column 1 is 34",
			args: args{
				row:    1,
				column: 1,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 10, 0, 0},
						Columns:  [4]uint{20, 14, 16, 13},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    true,
			wantErr: false,
		},
		{
			name: "should valid if column is 3 and row 1 and sum of column 2 and column 3 is 34",
			args: args{
				row:    1,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 34, 0, 0},
						Columns:  [4]uint{20, 14, 21, 13},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    true,
			wantErr: false,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of column 2 and column 3 is not 34",
			args: args{
				row:    1,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 34, 0, 0},
						Columns:  [4]uint{20, 14, 21, 12},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if column is 3 and row 1 and sum of row 1 is not 34",
			args: args{
				row:    1,
				column: 3,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 32, 0, 0},
						Columns:  [4]uint{20, 14, 21, 13},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    false,
			wantErr: false,
		},
		{
			name: "should invalid if column is 2 and row 1 and sum of row 1 is greater than 34",
			args: args{
				row:    1,
				column: 2,
				data: Data{
					Sum: Sum{
						Rows:     [4]uint{34, 35, 0, 0},
						Columns:  [4]uint{20, 14, 21, 13},
						Diagonal: [2]uint{18, 13},
					},
				}},
			want:    false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := validate(tt.args.row, tt.args.column, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("validate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
