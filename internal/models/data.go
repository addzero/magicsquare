package models

import (
	"fmt"
	"io"
	"os"
	"sync"
)

type Board [4][4]uint

var (
	outer io.Writer
	mutex sync.Mutex
)

func init() {
	outer = os.Stdout
}

func (b *Board) Print() {
	mutex.Lock()
	defer mutex.Unlock()
	for _, row := range b {
		for _, col := range row {
			fmt.Fprintf(outer, "%3d", col)
		}
		fmt.Fprintln(outer)
	}
	fmt.Fprintln(outer, "---------------------------")
}

func (d *Data) Print() {
	d.Sum.Print()
	mutex.Lock()
	defer mutex.Unlock()
	for _, row := range d.Board {
		for _, col := range row {
			fmt.Fprintf(outer, "%3d", col)
		}
		fmt.Fprintln(outer)
	}
	fmt.Fprintln(outer, "---------------------------")
}

type Sum struct {
	Rows     [4]uint
	Columns  [4]uint
	Diagonal [2]uint
}

func (s *Sum) Print() {
	mutex.Lock()
	defer mutex.Unlock()
	fmt.Fprintf(outer, "%9s: %v\n%9s: %v\n%9s: %v\n", "row", s.Rows, "columns", s.Columns, "diagonal", s.Diagonal)
}

type Data struct {
	Board  Board
	Remain []uint
	Sum    Sum
}
